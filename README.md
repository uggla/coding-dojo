> Coding-dojo "VRTICE Tech Lunch"

# Séances

* [13/09/2019](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2019-09-13-price) (\#1) : TDD / Calcul de prix
* [03/10/2019](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2019-10-03-price2) (\#2) : TDD / Calcul de prix (suite)
* [18/10/2019](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2019-10-18-amazon-test) (\#3) : TDD / Test Amazon, itinéraire de livraison
* [31/10/2019](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2019-10-31-tennis) (\#4) : TDD / Calcul des points de tennis (Fishbowl-mob-programming)
* [15/11/2019](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2019-11-15-npi-expression) (\#5) : TDD / Calculatrice à notation polonaise inversée
* [28/11/2019](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2019-11-28-mock) (\#6) : Mocks (sans librairie)
* [13/12/2019](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2019-12-13-mock) (\#7) : Mocks (avec librairie : Mockito)
* [10/01/2020](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/tree/2020-01-10-gildedrose-coverage) (\#8) : Code legacy (GildedRose) et ajout de tests unitaires
* [07/02/2020](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2020-02-07-gildedrose-refactoring) (\#9) : Code legacy (GildedRose) : refactoring
* [20/02/2020](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2020-02-20-promises) (\#10) : Programmation asynchrone, promesses et tests unitaires
* [06/03/2020](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2020-03-06-promises-2) (\#11) : Programmation asynchrone, promesses et tests unitaires (suite)
* [21/10/2021](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2021-10-21-fluent-api-for-price) : API fluent pour calcul de prix
* [04/11/2021](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2021-11-04-fluent-api-for-price-2) : API fluent pour calcul de prix : suite et API qui guide le dev
* [16/11/2021](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2021-11-16-mastermind) : Mastermind
* [24/11/2021](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2021-11-24-agile-grenoble-2021) : Démo coding-dojo à Agile Grenoble 2021
* [02/12/2021](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2021-12-02-mutations-testing) : Mutation testing
* [28/04/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-04-28-data-manipulation) : Manipulation de données (filter, map, ...)
* [08/09/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-09-08-diamond-kata) : Diamond Kata
* [22/09/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-09-22-cupcake-kata) : Cupcake kata
* [06/10/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-10-06-bowling-kata) : Bowling kata
* [14/10/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-10-20-roman-numerals) : Convertisseur de nombre régulier en nombre romain
* [03/11/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-11-03-diamond-kata-recursivity) : Diamond Kata avec récursivité
* [10/11/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-11-10-api-with-promises) : Promesses et tests asynchrones
* [01/12/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-12-01-api-with-promises-2) : Promesses et tests asynchrones (suite)
* [15/12/2022](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2022-12-15-price-calcul) : Calcul de prix
* [18/01/2023](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2023-01-18-movies-stats) : Statistiques de cinéma
* [16/01/2023](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2023-02-16-numbers-to-text) : Conversion de nombres en texte (Anglais)
* [23/03/2023](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2023-03-23-numbers-to-text-2) : Conversion de nombres en texte (Anglais) : suite et fin
* [08/06/2023](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2023-06-08-rpg-character) : RPG / Character
* [21/09/2023](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2023-09-21-game-2048) : Jeu 2048
* [18/10/2023](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2023-10-18-game-2048-2) : Jeu 2048 #2
* [25/01/2024](https://gitlab.com/vrtice/vrtice-tech-lunch/coding-dojo/-/tree/2024-01-25-langton-ant) : Langton's ant kata

# Documents

* [Session \#1 et introduction au TDD](https://1drv.ms/b/s!ApNPnRFKyu2fj9Iq8VTYTi-YL19cMw?e=rVyU0Q)
